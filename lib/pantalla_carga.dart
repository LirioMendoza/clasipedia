import 'package:clasi_pedia/music_inicio.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:splashscreen/splashscreen.dart';

class PantallaCarga extends StatefulWidget {
  PantallaCarga({Key key}) : super(key: key);

  @override
  _PantallaCargaState createState() => _PantallaCargaState();
}

class _PantallaCargaState extends State<PantallaCarga> {
  @override
  Widget build(BuildContext context) {
    return SplashScreen(
      seconds: 3,
      navigateAfterSeconds: MusicInicio(),
      title: Text(
        'Clasi-Pedia',
        style: GoogleFonts.abrilFatface(
          fontWeight: FontWeight.bold,
          fontSize: 60,
          color: Colors.black
        ),
      ),

      image: Image.asset('assets/logoMusic.png'),
      photoSize: 140,
      backgroundColor: Colors.amber[400],
    );
  }
}