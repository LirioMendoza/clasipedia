import 'package:clasi_pedia/mas_musicos.dart';
import 'package:clasi_pedia/pantalla_carga.dart';
import 'package:flutter/material.dart';
import 'bioX.dart';
import 'music_inicio.dart';
 
void main() => runApp(MyApp());
 
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Material App',
      debugShowCheckedModeBanner: false,
      routes: {
        'pantallaCarga' : (BuildContext context) => PantallaCarga(),
        'musicInicio' : (BuildContext context) => MusicInicio(),
        'biosMusic' : (BuildContext context) => BioX(), 
        'masMusicos' : (BuildContext context) => MasMusicos(), 
      },
      initialRoute: 'pantallaCarga',
    );
  }
}
