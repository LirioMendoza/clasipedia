import 'package:clasi_pedia/bioX.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:animated_text_kit/animated_text_kit.dart';

class MasMusicos extends StatefulWidget {
  MasMusicos({Key key}) : super(key: key);

  @override
  _MasMusicosState createState() => _MasMusicosState();
}

class _MasMusicosState extends State<MasMusicos> {

  //Datos Brahms
  String bioBrahms = """
Johannes Brahms (Hamburgo, 7 de mayo de 1833-Viena, 3 de abril de 1897) fue un compositor, pianista y director de orquesta alemán del romanticismo, considerado el más clásico de los compositores de dicho periodo. Nacido en Hamburgo en una familia luterana, pasó gran parte de su vida profesional en Viena.\n
Se mantuvo fiel toda su vida al clasicismo romántico y conservador, influenciado por Wolfgang Amadeus Mozart, Franz Joseph Haydn y, particularmente, por Ludwig van Beethoven. Fue posiblemente el mayor representante del círculo conservador en la Guerra de los románticos. Sus oponentes, los progresistas radicales de Weimar, estaban representados por Franz Liszt, los integrantes de la posteriormente llamada Nueva Escuela Alemana y por Richard Wagner. Sus escritores contemporáneos y posteriores lo han considerado como tradicionalista e innovador. Su música está firmemente arraigada en las estructuras y técnicas de composición de los maestros clásicos. Si bien muchos contemporáneos encontraron su música demasiado académica, su contribución y artesanía han sido admiradas por figuras posteriores tan diversas como Arnold Schönberg y Edward Elgar. La naturaleza diligente y altamente construida de sus obras fue un punto de partida y una inspiración para una generación de compositores. Sin embargo, incrustados dentro de sus meticulosas estructuras, hay motivos profundamente románticos.\n
Brahms compuso para orquesta sinfónica, conjuntos de cámara, piano, órgano y voz y coro. Fue un pianista virtuoso y estrenó muchas de sus propias obras. Trabajó con algunos de los principales artistas de su tiempo, incluida la pianista Clara Schumann y el violinista Joseph Joachim (los tres eran amigos cercanos). Muchas de sus obras se han convertido en elementos básicos del repertorio de conciertos moderno. Era un perfeccionista intransigente y destruyó algunas de sus obras y dejó otras inéditas.\n""";
  String obrasBrahms = """
Sinfonía n.º 1 \nSinfonía n.º 3 \nSinfonía n.º 4 \nObertura del festival académico \nObertura trágica\n""";

  //Datos Haydn
  String bioHaydn = """
Franz Joseph Haydn (Rohrau, cerca de Viena; 31 de marzo de 1732 - Viena; 31 de mayo de 1809), conocido como Joseph Haydn, fue un compositor austriaco. Es uno de los máximos representantes del periodo Clásico, además de ser conocido como el «padre de la sinfonía» y el «padre del cuarteto de cuerda» gracias a sus importantes contribuciones a ambos géneros. También contribuyó al desarrollo instrumental del trío con piano y en la evolución de la forma sonata.\n
Vivió durante toda su vida en Austria y desarrolló gran parte de su carrera como músico de corte para la rica y aristocrática familia Esterházy de Hungría. Aislado de otros compositores y tendencias musicales, hasta el último tramo de su vida, estuvo, según dijo: «forzado a ser original». En la época de su muerte, era uno de los compositores más célebres de toda Europa.\n
Era hermano de Michael Haydn, que también fue considerado un buen compositor, y de Johann Evangelist, un tenor. Tuvo una estrecha amistad con Wolfgang Amadeus Mozart, del que incluso se cree que llegó a ser mentor, y fue profesor de Ludwig van Beethoven. La lista completa de las obras del compositor se puede consultar en el catálogo Hoboken, sistema de ordenación creado por Anthony van Hoboken.\n""";
  String obrasHaydn = """
Sinfonía n.º 100 \nSinfonía n.º 101 \nDeutschlandlied \n""";

  //Datos Bach
  String bioBach = """
Johann Sebastian Bach (Eisenach, en la actual Turingia, Sacro Imperio Romano Germánico, 21 de marzo / 31 de marzo de 1685 - Leipzig, en la actual Sajonia, Sacro Imperio Romano Germánico, 17 de julio / 28 de julio de 1750) fue un compositor, organista, clavecinista, director de orquesta, violinista, violagambista, maestro de capilla, cantor y profesor alemán del período barroco.\n
Fue el miembro más importante de una de las familias de músicos más destacadas de la historia, con más de 35 compositores famosos. Tuvo una gran fama como organista y clavecinista en toda Europa por su gran técnica y capacidad de improvisar música al teclado. Además del órgano y del clavecín, tocaba el violín y la viola da gamba.\n
Su fecunda obra es considerada la cumbre de la música barroca; destaca en ella su profundidad intelectual, su perfección técnica y su belleza artística, además de la síntesis de los diversos estilos nacionales de su época y del pasado. Bach es considerado el último gran maestro del arte del contrapunto​ y fuente de inspiración e influencia para posteriores compositores y músicos, tales como como Joseph Haydn, Wolfgang Amadeus Mozart, Ludwig van Beethoven, Felix Mendelssohn, Robert Schumann y Frédéric Chopin, entre muchos otros.\n""";
  String obrasBach = """
Mein Herze schwimmt im Blut, BWV 199 \nSonatas y partitas para violín solo \nGottes Zeit ist die allerbeste Zeit, BWV 106 \nTocata y fuga en re menor, BWV 565 \nTocata y fuga en re menor, BWV 538 \nEl arte de la fuga \nSuites inglesas \nVariaciones Goldberg \nSchweigt stille, plaudert nicht\nInvenciones y Sinfonías\n""";

  //Datos Liszt
  String bioLiszt = """
Franz Liszt (Raiding, Imperio austríaco, 22 de octubre de 1811-Bayreuth, Imperio alemán, 31 de julio de 1886) fue un compositor austro-húngaro romántico, un virtuoso pianista, director de orquesta, profesor de piano, arreglista y seglar franciscano.\n
Liszt se hizo famoso en toda Europa durante el siglo XIX por su gran habilidad como intérprete. Sus contemporáneos afirmaban que era el pianista técnicamente más avanzado de su época y quizás el más grande de todos los tiempos.  También fue un importante e influyente compositor, un profesor de piano notable, un director de orquesta que contribuyó significativamente al desarrollo moderno del arte y un benefactor de otros compositores y artistas, intérpretes o ejecutantes, en particular de Richard Wagner, Hector Berlioz, Camille Saint-Saëns, Edvard Grieg y Aleksandr Borodín.\n
Como compositor, fue uno de los más destacados representantes de la Nueva Escuela Alemana («Neudeutsche Schule»). Compuso una extensa y variada cantidad de obras para piano (rapsodia, estudios, transcripciones, etc.), en estilo concertante para piano y orquesta y también una extensa producción orquestal. Influyó a sus contemporáneos y sucesores y anticipó algunas ideas y tendencias del siglo XX. Algunas de sus contribuciones más notables fueron la invención del poema sinfónico, desarrollando el concepto de transformación temática como parte de sus experimentos en la forma musical, y hacer desviaciones radicales en la armonía.\n""";
  String obrasLiszt = """
Hamlet \nSinfonía Dante \nMazeppa \nRapsodia húngara n.º 1 \nRapsodia húngara n.º 2\n""";

  //Datos Mahler
  String bioMahler = """
Gustav Mahler (Kaliště, Bohemia, antiguamente bajo administración del Imperio austríaco, actualmente República Checa, 7 de julio de 1860-Viena, 18 de mayo de 1911) fue un compositor y director de orquesta austriaco, cuyas obras se consideran, junto con las de Richard Strauss, las más importantes del postromanticismo.\n
En la primera década del siglo XX, Gustav Mahler fue uno de los más importantes directores de orquesta y de ópera de su momento. Después de graduarse en el Conservatorio de Viena en 1878, fue sucesivamente director de varias orquestas cada vez más importantes en diversos teatros de ópera europeos, llegando en 1897 a la que entonces se consideraba la más notable: la dirección de la Ópera de la Corte de Viena (Hofoper). Durante sus diez años en la capital austriaca, Mahler —judío converso al catolicismo— sufrió la oposición y hostilidad de la prensa antisemita. Sin embargo, gracias a sus innovadoras producciones y a la insistencia en los más altos niveles de representación, se granjeó el reconocimiento como uno de los más grandes directores de ópera, particularmente como intérprete de las óperas de Richard Wagner y de Wolfgang Amadeus Mozart. Posteriormente, fue director de la Metropolitan Opera House y de la Orquesta Filarmónica de Nueva York.\n
Como compositor, centró sus esfuerzos en la forma sinfónica y en el lied. La Segunda, Tercera, Cuarta y Octava sinfonías y Das Lied von der Erde (La canción de la Tierra) conjugaron en sus partituras ambos géneros. Él mismo advertía que componer una sinfonía era «construir un mundo con todos los medios posibles», por lo que sus trabajos en este campo se caracterizan por una amplísima heterogeneidad.\n
La revalorización de Mahler fue lenta, al igual que la de Anton Bruckner, y se vio retrasada a causa de su gran originalidad y del auge del nazismo en Alemania y Austria, pues su condición de judío catalogó a su obra como «degenerada» y «moderna». Lo mismo sucedió con otros compositores, caídos en desgracia en el Tercer Reich. Solo al final de la Segunda Guerra Mundial y por la decidida labor de directores como Bruno Walter, Otto Klemperer y, más tarde, Bernard Haitink o Leonard Bernstein, su música empezó a interpretarse con más frecuencia en el repertorio de las grandes orquestas, encontrándose entre los compositores más destacados en la historia de la música.\n""";
  String obrasMahler = """
Sinfonía n.º 1 \nSinfonía n.º 8 \nSinfonía n.º 2 \n""";

  //Datos Mendelssohn
  String bioMendelssohn = """
Felix Mendelssohn (Hamburgo, 3 de febrero de 1809-Leipzig, 4 de noviembre de 1847), cuyo nombre completo era Jakob Ludwig Felix Mendelssohn Bartholdy, fue un compositor, director de orquesta y pianista de música romántica alemán, y hermano de la también pianista y compositora Fanny Mendelssohn.\n
En su infancia fue considerado un prodigio musical, pero sus padres no trataron de sacar partido de sus habilidades. De hecho, su padre declinó la oportunidad de que Felix siguiera una carrera musical hasta que quedó claro que tenía la firme intención de dedicarse seriamente a ella.\n
Al temprano éxito en Alemania, le siguió un viaje a través de toda Europa; Mendelssohn fue recibido particularmente bien en Inglaterra como compositor, director y solista, y sus diez visitas allí, durante las que estrenó la mayoría de sus obras, formaron una parte importante de su carrera adulta. Fundó el Conservatorio de Leipzig, un bastión de su lucha contra las perspectivas musicales radicales de algunos de sus contemporáneos.\n
Las obras de Mendelssohn incluyen sinfonías, conciertos, oratorios, oberturas, música incidental, música para piano, música para órgano y música de cámara. También tuvo un importante papel en el resurgimiento del interés en la obra de Johann Sebastian Bach. Sus gustos musicales esencialmente conservadores lo separaron de muchos de sus contemporáneos más aventurados, como Franz Liszt, Richard Wagner o Hector Berlioz. Después de un largo periodo de denigración debido al cambio de los gustos musicales y el antisemitismo a finales del siglo XIX y comienzos del XX, su originalidad creativa tiene un buen reconocimiento, ha sido evaluada de nuevo y se ha convertido en uno de los compositores más populares del periodo romántico.\n""";
  String obrasMendelssohn = """
El sueño de una noche de verano \nSinfonía n.º 3 \nSinfonía n.º 4 \nConcierto para violín \nLas Hébridas \nPaulus \nElías \n""";

  //Datos Paganini
  String bioPaganini = """
Niccolò Paganini (Génova, 27 de octubre de 1782-Niza, 27 de mayo de 1840) fue un compositor italiano. Es considerado uno de los arquetipos del virtuosismo del violín y máximo representante del movimiento instrumental del Romanticismo. Contribuyó con sus aportaciones al desarrollo de la "técnica moderna del violín".\n
Niño prodigio, antes de cumplir los catorce años dominaba ya todos los secretos del violín, al extremo de que sus profesores reconocían no tener nada más que enseñarle. La gira que emprendió en 1828 por ciudades como Viena, Praga, Varsovia y Berlín lo consagró como el mejor violinista de su tiempo, capaz de extraer al instrumento músico sonidos y efectos inconcebibles, y le valió la fascinada admiración de personalidades como Franz Liszt.\n
Además del violín compuso música para mandolina, guitarra, viola y fagot. Destacan sus duetos para violín y guitarra y sus composiciones para cuarteto de cuerdas.\n""";
  String obrasPaganini = """
24 Caprichos para violín \nConcierto para violín n.º 1 \nLa danza de las brujas \nTarantella""";

  //Datos Schumann
  String bioSchumann = """
Robert Schumann (8 de junio de 1810, Zwickau - 29 de julio de 1856, Endenich, hoy en día Bonn) fue un compositor, pianista y crítico musical alemán del siglo xix. Es considerado uno de los más importantes y representativos compositores del Romanticismo musical.\n
Schumann dejó sus estudios de derecho, con la intención de seguir una carrera como virtuoso pianista. Su maestro Friedrich Wieck le había asegurado que podría convertirse en el mejor pianista de Europa, pero una lesión en la mano terminó este sueño y centró sus energías musicales en la composición. Se casó con la hija de Wieck, Clara, después de una larga y dura batalla legal con el padre. Comenzó una asociación musical para toda la vida, ya que Clara era una pianista y prodigio musical consolidada. Clara y Robert también mantuvieron una estrecha relación con el compositor alemán Johannes Brahms.\n
Hasta 1840, Schumann compuso exclusivamente obras para piano. Más tarde, escribió para piano y orquesta y muchos lieder. Compuso cuatro sinfonías, una ópera y otras obras orquestales, corales y de cámara.\n
Schumann sufrió una enfermedad mental que se manifestó por primera vez en 1833 como un episodio depresivo melancólico severo, que se repitió varias veces alternando con fases de «exaltación» y cada vez con más ideas delirantes de ser envenenado o amenazado con elementos metálicos. Lo que ahora se cree que fue una combinación de trastorno bipolar y quizás envenenamiento por mercurio condujo a períodos «maníacos» y «depresivos» en la productividad compositiva de Schumann. Después de un intento de suicidio en 1854, Schumann fue admitido a petición propia en un hospital psiquiátrico en Endenich, cerca de Bonn. Diagnosticado con melancolía psicótica, murió de neumonía dos años después a la edad de 46 años, sin recuperarse de su enfermedad mental. Su vida y obra reflejan en su máxima expresión la naturaleza del Romanticismo: pasión, drama y alegría. En sus obras, de gran intensidad lírica, confluyen una notable complejidad musical con la íntima unión de música y texto.\n""";
  String obrasSchumann = """
Sinfonía n.º 1 \nSinfonía nº. 2 \nSinfonía n.º 3 \nKinderszenen\n""";

  //Datos Stravinsky
  String bioStravinsky = """
Ígor Fiódorovich Stravinski (Oranienbaum, 17 de junio de 1882-Nueva York, 6 de abril de 1971) fue un compositor y director de orquesta ruso, fue uno de los músicos más importantes y trascendentes del siglo XX.\n
Su larga vida le permitió conocer gran variedad de corrientes musicales. Resultan justificadas sus protestas contra quienes lo tildaban como un músico del porvenir: «Es algo absurdo. No vivo en el pasado ni en el futuro. Estoy en el presente». En su presente compuso una gran cantidad de obras clásicas abordando varios estilos como el primitivismo, el neoclasicismo y el serialismo, pero es conocido mundialmente sobre todo por tres obras de uno de sus períodos iniciales —el llamado «período ruso»—: El pájaro de fuego (L'Oiseau de feu, 1910), Petrushka (1911) y La consagración de la primavera (Le sacre du printemps, 1913). Para muchos, estos ballets clásicos, atrevidos e innovadores, prácticamente reinventaron el género. Stravinski también escribió para diversos tipos de conjuntos en un amplio espectro de formas clásicas, desde óperas y sinfonías a pequeñas piezas para piano y obras para grupos de jazz.\n
Stravinski también alcanzó fama como pianista y director, frecuentemente de sus propias composiciones. Fue también escritor; con la ayuda de Alexis Roland-Manuel, Stravinski compiló un trabajo teórico titulado Poetics of Music (Poética musical), en el cual dijo una famosa frase: «La música es incapaz de expresar nada por sí misma».\n 
Esencialmente un ruso cosmopolita, Stravinski fue uno de los compositores y artistas más influyentes de la música del siglo XX, tanto en Occidente como en su tierra natal. Fue considerado por la revista Time como una de las personalidades más influyentes del siglo XX.\n""";
  String obrasStravinsky = """
La consagración de la primavera \nPetrushka \nEl pájaro de fuego\n""";

  //Datos Tchaikovsky
  String bioTchaikovsky = """
Piotr Ilich Chaikovski (Votkinsk, Rusia, 1840 - San Petersburgo, 1893) fue un compositor ruso del período del Romanticismo.\n
Nacido en una familia de clase media, Chaikovski fue el segundo hijo de lliá Petróvich Chaikovski, su padre era director de una fábrica minera, sus padres eran aficionados a la música, la educación que recibió Chaikovski estaba dirigida a prepararle como funcionario, a pesar del interés musical que mostró. En contra de los deseos de su familia, decidió seguir una carrera musical y en 1862 accedió al Conservatorio de San Petersburgo, graduándose en 1865. La formación que recibió, formal y orientada al estilo musical occidental, lo apartó del movimiento contemporáneo nacionalista personificado en el «Grupo de los Cinco» conformado por un grupo de jóvenes compositores rusos, con los cuales Chaikovski mantuvo una relación profesional y de amistad a lo largo de su carrera.\n
Mientras desarrollaba su estilo, Chaikovski escribió música en varios géneros y formas, incluyendo la sinfonía, ópera, ballet, música instrumental, de cámara y la canción. A pesar de contar con varios éxitos, nunca tuvo mucha confianza o seguridad en sí mismo y su vida estuvo salpicada por crisis personales y períodos de depresión. Como factores que quizá contribuyeron a esto, pueden mencionarse su homosexualidad reprimida y el miedo a que se revelara su condición, su desastroso matrimonio que contrajo con Antonina Miliukova en 1877, que duró menos de un mes, y su asociación de catorce años con la rica viuda Nadezhda von Meck. En medio de esta agitada vida personal, la reputación de Chaikovski aumentó; recibió honores por parte del zar, obtuvo una pensión vitalicia y fue alabado en las salas de conciertos de todo el mundo. Su repentina muerte a los cincuenta y tres años en el año 1893 suele atribuirse generalmente al cólera, pero algunos lo atribuyen a un suicidio.\n
A pesar de ser popular en todo el mundo, Chaikovski recibió a veces duras críticas por parte de críticos y compositores. Sin embargo, su reputación como compositor es hoy en día segura, y ha desaparecido por completo el desdén con el que los críticos occidentales a principios y mediados del siglo XX catalogaban su música como vulgar y falta de pensamiento.\n
De hecho, Chaikovski está considerado actualmente como el más destacado músico de Rusia y una de las figuras más importantes de la cultura de ese país a lo largo de su historia.\n""";
  String obrasTchaikovsky = """
El lago de los cisnes \nEl cascanueces \nLa bella durmiente \nSinfonía n.º 6 \nConcierto para piano n.º 1 \nObertura 1812 \nConcierto para violín \nEugenio Onegin \nLa dama de picas\n""";

  //Datos Vivaldi
  String bioVivaldi = """
Antonio Lucio Vivaldi (Venecia, 4 de marzo de 1678-Viena, 28 de julio de 1741) fue un compositor, violinista, impresario, profesor y sacerdote católico veneciano del barroco. Era apodado Il prete rosso («El cura rojo») por ser sacerdote y pelirrojo. Su maestría se refleja en haber cimentado el género del concierto, el más importante de su época. Compuso unas 770 obras, entre las cuales se cuentan más de 400 conciertos y cerca de 46 óperas. Es especialmente conocido, a nivel popular, por ser el autor de la serie de conciertos para violín y orquesta Las cuatro estaciones.\n
Hijo del violinista Giovanni Battista Vivaldi, el pequeño Antonio se inició en el mundo de la música probablemente de la mano de su padre. Orientado hacia la carrera eclesiástica, fue ordenado sacerdote en 1703, aunque sólo un año más tarde se vio obligado a renunciar a celebrar misa a consecuencia de una enfermedad bronquial, posiblemente asma.\n
También en 1703 ingresó como profesor de violín en el Pio Ospedale della Pietà, una institución dedicada a la formación musical de muchachas huérfanas. Ligado durante largos años a ella, muchas de sus composiciones fueron interpretadas por primera vez por su orquesta femenina. En este marco vieron la luz sus primeras obras, como las Suonate da camera Op. 1, publicadas en 1705, y los doce conciertos que conforman la colección L'estro armonico Op. 3, publicada en Ámsterdam en 1711.\n
Antonio Vivaldi alcanzó en poco tiempo renombre en todo el territorio italiano, desde donde su nombradía se extendió al resto del continente europeo, y no sólo como compositor, sino también, y no en menor medida, como violinista, pues fue uno de los más grandes de su tiempo.\n""";
  String obrasVivaldi = """
Las cuatro estaciones \nOrlando furioso""";

  //Datos Wagner
  String bioWagner = """
Wilhelm Richard Wagner (Leipzig, Reino de Sajonia, Confederación del Rin, 22 de mayo de 1813-Venecia, Reino de Italia, 13 de febrero de 1883) fue un compositor, director de orquesta, poeta, ensayista, dramaturgo y teórico musical alemán del Romanticismo. Destacan principalmente sus óperas (calificadas como «dramas musicales» por el propio compositor) en las que, a diferencia de otros compositores, asumió también el libreto y la escenografía.\n
Transformó el pensamiento musical con la idea de la «obra de arte total» (Gesamtkunstwerk), la síntesis de todas las artes poéticas, visuales, musicales y escénicas, que desarrolló en una serie de ensayos entre 1849 y 1852, y que plasmó en la primera mitad de su monumental tetralogía El anillo del nibelungo. Sin embargo, sus ideas sobre la relación entre la música y el teatro cambiaron nuevamente y reintrodujo algunas formas operísticas tradicionales en las obras de su última etapa, como en Los maestros cantores de Núremberg.\n
Su ópera Tristán e Isolda se describe a veces como punto de inicio de la música académica contemporánea. La influencia de Wagner se extendió también a la filosofía, la literatura, las artes visuales y el teatro. Hizo construir su propio teatro de ópera, el Festspielhaus de Bayreuth, para escenificar sus obras del modo en que él las imaginaba y que contienen diseños novedosos. Allí tuvo lugar el estreno de la tetralogía del Anillo y Parsifal, donde actualmente se siguen representando sus obras operísticas más importantes en un Festival anual a cargo de sus descendientes. Los puntos de vista de Wagner sobre la dirección orquestal también fueron muy influyentes. Escribió ampliamente sobre música, teatro y política, obras que han sido objeto de debate en las últimas décadas, especialmente algunas de contenido antisemita, como su ensayo El judaísmo en la música y por su supuesta influencia sobre Adolf Hitler y el nacionalsocialismo.\n
Wagner logró todo esto a pesar de una vida que se caracterizó, hasta sus últimas décadas, por el exilio político, relaciones amorosas turbulentas, pobreza y repetidas huidas de sus acreedores. Su agresiva personalidad y sus opiniones, con frecuencia demasiado directas, sobre la música, la política y la sociedad lo convirtieron en un personaje polémico, etiqueta que todavía mantiene. El impacto de sus ideas se puede encontrar en muchas de las artes del siglo.\n""";
  String obrasWagner = """
El holandés errante \nTristán e Isolda \nTannhäuser \nEl oro del Rin \nLohengrin""";

  @override
  Widget build(BuildContext context) {
    return Container(
      child:Scaffold(
        body: Stack(
          children: [
            fondoPantalla(),
            SafeArea(
              child: Column(
                children: [
                  _miAppBar(context),
                  _tablaMusicos(),
                ],
              ),
            )
          ],
        )
      ) ,
    );
  }

  Widget fondoPantalla(){
    return Container(
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment(0.0,0.0),
          end: Alignment (0.5,0.9),
          colors: <Color>[
            Colors.amber[300],
            Colors.amber[100],
          ]
        )
      ),
    );
  }

  Widget _miAppBar(BuildContext context){
    return Container(
      height: 55,
      decoration: BoxDecoration(
        color: Colors.amber[600]
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children:<Widget> [
          Container(
            width: 50,
            child: FlatButton(
              padding: EdgeInsets.only(left:10, top: 10),
              onPressed: (){
                Navigator.pop(context);
              },
              child: Icon(
                Icons.arrow_back,
                size: 35,
                color: Colors.black,
              ),
            ),
          ),
          SizedBox(width: 25),
          _fadeText()  
        ],
      ),
    );
  }

  Widget _fadeText(){
    return Center(
      child: Container(
        child: ColorizeAnimatedTextKit(
          repeatForever: true, 
          text: [
            "Clasi-Pedia",
            "Más Músicos",
          ],
          textStyle: GoogleFonts.abrilFatface(
            fontSize: 30.0, 
            fontWeight: FontWeight.w600,
          ),
          colors: [
            Colors.grey[900],
            Colors.grey[850],
            Colors.grey[800],
            Colors.grey[700],
          ],
          textAlign: TextAlign.center,
          alignment: AlignmentDirectional.topStart // or Alignment.topLeft
        ),
      ),
    );
  }

  Widget _tablaMusicos(){
    return Table(
          children: [
            TableRow(
              children: [
                Container(
                  margin: EdgeInsets.all(5),
                  width: 150,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage('assets/Musicos/jbrahms.jpg'),
                      fit: BoxFit.cover,
                    ),
                    borderRadius: BorderRadius.circular(10),
                  ),

                  child: Container(
                    child: FlatButton(
                      onPressed: (){
                        Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (context)=> BioX(
                              musico: 'J. Brahms',
                              nombreCompleto: '\nJohannes Brahms\n',
                              bioMusico: bioBrahms,
                              fotoMusico: 'assets/Musicos/jbrahms.jpg',
                              obras: obrasBrahms,
                            )
                          )
                        );
                      },

                      child: Padding(
                        padding: EdgeInsets.only(top: 130, bottom: 5),
                          child: Container(
                            width: 100,
                            decoration: BoxDecoration(
                              color: Colors.purple[400],
                              borderRadius: BorderRadius.circular(20),
                            ),
                            child: Center(
                              child: Text(
                                'Brahms',
                                style: GoogleFonts.alegreya(
                                  fontSize: 15,
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                          ),
                      ),
                    )
                  ),

                ),
                //Container Brahms

                Container(
                  margin: EdgeInsets.all(5),
                  width: 150,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage('assets/Musicos/josephHaydn.jpg'),
                      fit: BoxFit.cover,
                    ),
                    borderRadius: BorderRadius.circular(10),
                  ),

                  child: Container(
                    child: FlatButton(
                      onPressed: (){
                        Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (context)=> BioX(
                              musico: 'J. Haydn',
                              nombreCompleto: '\nFranz Joseph Haydn\n',
                              bioMusico: bioHaydn,
                              fotoMusico: 'assets/Musicos/josephHaydn.jpg',
                              obras: obrasHaydn,
                            )
                          )
                        );
                      },

                      child: Padding(
                        padding: EdgeInsets.only(top: 130, bottom: 5),
                        child: Container(
                          width: 100,
                          decoration: BoxDecoration(
                            color: Colors.purple[400],
                            borderRadius: BorderRadius.circular(20),
                          ),
                          child: Center(
                            child: Text(
                              'J. Haydn',
                              style: GoogleFonts.alegreya(
                                fontSize: 15,
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                        ),
                      ),
                    )
                  ),

                ),
                //Container Haydn

                Container(
                  margin: EdgeInsets.all(5),
                  width: 150,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage('assets/Musicos/jsbach.jpg'),
                      fit: BoxFit.cover,
                    ),
                    borderRadius: BorderRadius.circular(10),
                  ),

                  child: Container(
                    child: FlatButton(
                      onPressed: (){
                        Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (context)=> BioX(
                              musico: 'J. S. Bach',
                              nombreCompleto: '\nJohann Sebastian Bach\n',
                              bioMusico: bioBach,
                              fotoMusico: 'assets/Musicos/jsbach.jpg',
                              obras: obrasBach,
                            )
                          )
                        );
                      },  
                      child: Padding(
                        padding: EdgeInsets.only(top: 130, bottom: 5),
                        child: Container(
                          width: 100,
                          decoration: BoxDecoration(
                            color: Colors.purple[400],
                            borderRadius: BorderRadius.circular(20),
                          ),
                          child: Center(
                            child: Text(
                              'Bach',
                              style: GoogleFonts.alegreya(
                                fontSize: 15,
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                        ),
                      ),
                    )
                  ),

                ),
                  //Container Bach
              ]
            ),

            TableRow(
              children: [
                Container(
                  margin: EdgeInsets.all(5),
                  width: 150,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage('assets/Musicos/liszt.png'),
                      fit: BoxFit.cover,
                    ),
                    borderRadius: BorderRadius.circular(10),
                  ),

                  child: Container(
                    child: FlatButton(
                      onPressed: (){
                        Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (context)=> BioX(
                              musico: 'F. Liszt',
                              nombreCompleto: '\nFranz Liszt\n',
                              bioMusico: bioLiszt,
                              fotoMusico: 'assets/Musicos/liszt.png',
                              obras: obrasLiszt,
                            )
                          )
                        );
                      },

                      child: Padding(
                        padding: EdgeInsets.only(top: 130, bottom: 5),
                          child: Container(
                            width: 100,
                            decoration: BoxDecoration(
                              color: Colors.purple[400],
                              borderRadius: BorderRadius.circular(20),
                            ),
                            child: Center(
                              child: Text(
                                'Liszt',
                                style: GoogleFonts.alegreya(
                                  fontSize: 15,
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                          ),
                      ),
                    )
                  ),

                ),
                //Container Liszt

                Container(
                  margin: EdgeInsets.all(5),
                  width: 150,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage('assets/Musicos/mahler.jpg'),
                      fit: BoxFit.cover,
                    ),
                    borderRadius: BorderRadius.circular(10),
                  ),

                  child: Container(
                    child: FlatButton(
                      onPressed: (){
                        Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (context)=> BioX(
                              musico: 'G. Mahler',
                              nombreCompleto: '\nGustav Mahler\n',
                              bioMusico: bioMahler,
                              fotoMusico: 'assets/Musicos/mahler.jpg',
                              obras: obrasMahler,
                            )
                          )
                        );
                      },

                      child: Padding(
                        padding: EdgeInsets.only(top: 130, bottom: 5),
                        child: Container(
                          width: 100,
                          decoration: BoxDecoration(
                            color: Colors.purple[400],
                            borderRadius: BorderRadius.circular(20),
                          ),
                          child: Center(
                            child: Text(
                              'Mahler',
                              style: GoogleFonts.alegreya(
                                fontSize: 15,
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                        ),
                      ),
                    )
                  ),

                ),
                //Container Mahler

                Container(
                  margin: EdgeInsets.all(5),
                  width: 150,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage('assets/Musicos/mendelssohn.jpg'),
                      fit: BoxFit.cover,
                    ),
                    borderRadius: BorderRadius.circular(10),
                  ),

                  child: Container(
                    child: FlatButton(
                      onPressed: (){
                        Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (context)=> BioX(
                              musico: 'F. Mendelssohn',
                              nombreCompleto: '\nFelix Mendelssohn\n',
                              bioMusico: bioMendelssohn,
                              fotoMusico: 'assets/Musicos/mendelssohn.jpg',
                              obras: obrasMendelssohn,
                            )
                          )
                        );
                      },  
                      child: Padding(
                        padding: EdgeInsets.only(top: 130, bottom: 5),
                        child: Container(
                          width: 100,
                          decoration: BoxDecoration(
                            color: Colors.purple[400],
                            borderRadius: BorderRadius.circular(20),
                          ),
                          child: Center(
                            child: Text(
                              'Mendelssohn',
                              style: GoogleFonts.alegreya(
                                fontSize: 15,
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                        ),
                      ),
                    )
                  ),

                ),
                //Container Mendelssohn
              ]
            ),

            TableRow(
              children: [
                Container(
                  margin: EdgeInsets.all(5),
                  width: 150,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage('assets/Musicos/paganini.jpg'),
                      fit: BoxFit.cover,
                    ),
                    borderRadius: BorderRadius.circular(10),
                  ),

                  child: Container(
                    child: FlatButton(
                      onPressed: (){
                        Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (context)=> BioX(
                              musico: 'N. Paganini',
                              nombreCompleto: '\nNiccolò Paganini\n',
                              bioMusico: bioPaganini,
                              fotoMusico: 'assets/Musicos/paganini.jpg',
                              obras: obrasPaganini,
                            )
                          )
                        );
                      },

                      child: Padding(
                        padding: EdgeInsets.only(top: 130, bottom: 5),
                          child: Container(
                            width: 100,
                            decoration: BoxDecoration(
                              color: Colors.purple[400],
                              borderRadius: BorderRadius.circular(20),
                            ),
                            child: Center(
                              child: Text(
                                'Paganini',
                                style: GoogleFonts.alegreya(
                                  fontSize: 15,
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                          ),
                      ),
                    )
                  ),

                ),
                //Container Paganini

                Container(
                  margin: EdgeInsets.all(5),
                  width: 150,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage('assets/Musicos/schumann.jpg'),
                      fit: BoxFit.cover,
                    ),
                    borderRadius: BorderRadius.circular(10),
                  ),

                  child: Container(
                    child: FlatButton(
                      onPressed: (){
                        Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (context)=> BioX(
                              musico: 'R. Schumann',
                              nombreCompleto: '\nRobert Schumann\n',
                              bioMusico: bioSchumann,
                              fotoMusico: 'assets/Musicos/schumann.jpg',
                              obras: obrasSchumann,
                            )
                          )
                        );
                      },

                      child: Padding(
                        padding: EdgeInsets.only(top: 130, bottom: 5),
                        child: Container(
                          width: 100,
                          decoration: BoxDecoration(
                            color: Colors.purple[400],
                            borderRadius: BorderRadius.circular(20),
                          ),
                          child: Center(
                            child: Text(
                              'R. Schumann',
                              style: GoogleFonts.alegreya(
                                fontSize: 15,
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                        ),
                      ),
                    )
                  ),

                ),
                //Container Schumann

                Container(
                  margin: EdgeInsets.all(5),
                  width: 150,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage('assets/Musicos/stravinsky.jpg'),
                      fit: BoxFit.cover,
                    ),
                    borderRadius: BorderRadius.circular(10),
                  ),

                  child: Container(
                    child: FlatButton(
                      onPressed: (){
                        Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (context)=> BioX(
                              musico: 'Stravinski',
                              nombreCompleto: '\nÍgor Stravinski\n',
                              bioMusico: bioStravinsky,
                              fotoMusico: 'assets/Musicos/stravinsky.jpg',
                              obras: obrasStravinsky,
                            )
                          )
                        );
                      },  
                      child: Padding(
                        padding: EdgeInsets.only(top: 130, bottom: 5),
                        child: Container(
                          width: 100,
                          decoration: BoxDecoration(
                            color: Colors.purple[400],
                            borderRadius: BorderRadius.circular(20),
                          ),
                          child: Center(
                            child: Text(
                              'Stravinski',
                              style: GoogleFonts.alegreya(
                                fontSize: 15,
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                        ),
                      ),
                    )
                  ),

                ),
                //Container Stravinski
              ]
            ),

            TableRow(
              children: [
                Container(
                  margin: EdgeInsets.all(5),
                  width: 150,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage('assets/Musicos/tchaikovsky.jpg'),
                      fit: BoxFit.cover,
                    ),
                    borderRadius: BorderRadius.circular(10),
                  ),

                  child: Container(
                    child: FlatButton(
                      onPressed: (){
                        Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (context)=> BioX(
                              musico: 'Chaikovski',
                              nombreCompleto: '\nPiotr Ilich Chaikovski\n',
                              bioMusico: bioTchaikovsky,
                              fotoMusico: 'assets/Musicos/tchaikovsky.jpg',
                              obras: obrasTchaikovsky,
                            )
                          )
                        );
                      },

                      child: Padding(
                        padding: EdgeInsets.only(top: 130, bottom: 5),
                          child: Container(
                            width: 100,
                            decoration: BoxDecoration(
                              color: Colors.purple[400],
                              borderRadius: BorderRadius.circular(20),
                            ),
                            child: Center(
                              child: Text(
                                'Chaikovski',
                                style: GoogleFonts.alegreya(
                                  fontSize: 15,
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                          ),
                      ),
                    )
                  ),

                ),
                //Container Chaikovski

                Container(
                  margin: EdgeInsets.all(5),
                  width: 150,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage('assets/Musicos/vivaldi.jpg'),
                      fit: BoxFit.cover,
                    ),
                    borderRadius: BorderRadius.circular(10),
                  ),

                  child: Container(
                    child: FlatButton(
                      onPressed: (){
                        Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (context)=> BioX(
                              musico: 'Vivaldi',
                              nombreCompleto: '\nAntonio Vivaldi\n',
                              bioMusico: bioVivaldi,
                              fotoMusico: 'assets/Musicos/vivaldi.jpg',
                              obras: obrasVivaldi,
                            )
                          )
                        );
                      },

                      child: Padding(
                        padding: EdgeInsets.only(top: 130, bottom: 5),
                        child: Container(
                          width: 100,
                          decoration: BoxDecoration(
                            color: Colors.purple[400],
                            borderRadius: BorderRadius.circular(20),
                          ),
                          child: Center(
                            child: Text(
                              'Vivaldi',
                              style: GoogleFonts.alegreya(
                                fontSize: 15,
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                        ),
                      ),
                    )
                  ),

                ),
                //Container Vivaldi

                Container(
                  margin: EdgeInsets.all(5),
                  width: 150,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage('assets/Musicos/wagner.jpg'),
                      fit: BoxFit.cover,
                    ),
                    borderRadius: BorderRadius.circular(10),
                  ),

                  child: Container(
                    child: FlatButton(
                      onPressed: (){
                        Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (context)=> BioX(
                              musico: 'Wagner',
                              nombreCompleto: '\nRichard Wagner\n',
                              bioMusico: bioWagner,
                              fotoMusico: 'assets/Musicos/wagner.jpg',
                              obras: obrasWagner,
                            )
                          )
                        );
                      },  
                      child: Padding(
                        padding: EdgeInsets.only(top: 130, bottom: 5),
                        child: Container(
                          width: 100,
                          decoration: BoxDecoration(
                            color: Colors.purple[400],
                            borderRadius: BorderRadius.circular(20),
                          ),
                          child: Center(
                            child: Text(
                              'Wagner',
                              style: GoogleFonts.alegreya(
                                fontSize: 15,
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                        ),
                      ),
                    )
                  ),

                ),
                //Container Wagner
              ]
            ),
          ],
    );
  } 

}