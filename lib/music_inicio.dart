import 'package:clasi_pedia/bioX.dart';
import 'package:clasi_pedia/mas_musicos.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:animated_text_kit/animated_text_kit.dart';


class MusicInicio extends StatefulWidget {

  @override
  _MusicInicioState createState() => _MusicInicioState();
}

class _MusicInicioState extends State<MusicInicio> {
 
 //Datos Mozart
  String urlMozart = "https://i.ytimg.com/vi/YFekmXBUC08/maxresdefault.jpg";
  String bioMozart = "Wolfgang Amadeus Mozart, fue un compositor, pianista, director de orquesta y profesor del antiguo Arzobispado de Salzburgo, maestro del Clasicismo, considerado como uno de los músicos más influyentes y destacados de la historia.";
  String bioMozartComp = """
Johannes Chrysostomus Wolfgangus Theophilus Mozarta​ (Salzburgo, 27 de enero de 1756-Viena, 5 de diciembre de 1791), más conocido como Wolfgang Amadeus Mozart, fue un compositor, pianista, director de orquesta y profesor del antiguo Arzobispado de Salzburgo (anteriormente parte del Sacro Imperio Romano Germánico, actualmente parte de Austria), maestro del Clasicismo, considerado como uno de los músicos más influyentes y destacados de la historia.\n
Su obra abarca todos los géneros musicales de su época e incluye más de seiscientas creaciones, en su mayoría reconocidas como obras maestras de la música sinfónica, concertante, de cámara, para fortepiano, operística y coral, logrando una popularidad y difusión internacional.\n
En su niñez más temprana en Salzburgo, Mozart mostró una capacidad prodigiosa en el dominio de instrumentos de teclado y del violín. Con tan solo cinco años ya componía obras musicales y sus interpretaciones eran del aprecio de la aristocracia y realeza europea. A los diecisiete años fue contratado como músico en la corte de Salzburgo, pero su inquietud le llevó a viajar en busca de una mejor posición, siempre componiendo de forma prolífica. Durante su visita a Viena en 1781, tras ser despedido de su puesto en la corte, decidió instalarse en esta ciudad, donde alcanzó la fama que mantuvo el resto de su vida, a pesar de pasar por situaciones financieras difíciles. En sus años finales, compuso muchas de sus sinfonías, conciertos y óperas más conocidas, así como su Réquiem. Las circunstancias de su temprana muerte han sido objeto de numerosas especulaciones y elevadas a la categoría de mito.\n""";
  String obrasMozart = """La flauta mágica\nLas bodas de Fígaro\n Don Giovanni\nCosì fan tutte\nEl rapto en el serrallo\n Sinfonía n.º 40\nSerenata n.º 13\nRéquiem\n""";

  //Datos Beethoven
String bioBeethoven ="""
Ludwig van Beethoven1​ (Bonn, Arzobispado de Colonia; 16 de diciembre de 17702​-Viena, 26 de marzo de 1827) fue un compositor, director de orquesta, pianista y profesor de piano alemán. Su legado musical abarca, cronológicamente, desde el Clasicismo hasta los inicios del Romanticismo. Es considerado uno de los compositores más importantes de la historia de la música y su legado ha influido de forma decisiva en la evolución posterior de este arte.\n
Siendo el último gran representante del clasicismo vienés (después de Christoph Willibald Gluck, Joseph Haydn y Wolfgang Amadeus Mozart), Beethoven consiguió hacer trascender la música del Romanticismo, influyendo en diversidad de obras musicales del siglo XIX. Su arte se expresó en numerosos géneros y aunque las sinfonías fueron la fuente principal de su popularidad internacional, su impacto resultó ser principalmente significativo en sus obras para piano y música de cámara.\n
Su producción incluye los géneros pianístico (treinta y dos sonatas para piano), de cámara (incluyendo numerosas obras para conjuntos instrumentales de entre ocho y dos miembros), concertante (conciertos para piano, para violín y triple), sacra (dos misas, un oratorio), lieder, música incidental (la ópera Fidelio, un ballet, músicas para obras teatrales), y orquestal, en la que ocupan lugar preponderante Nueve sinfonías.\n""";
  String obrasBeethoven = """Para Elisa.\nSinfonía n.º 9\nSonata para piano n.º 14\nMissas\nSonata para piano n.º 8\nSinfonía n.º 5\nSinfonía n.º 6\nSonata para piano n.º 21\nSonata para piano n.º 23\nSonata para violín n.º 9\nSinfonía n.º 3\nFidelio\n""";

  //Datos Bizet
  String bioBizet ="""
Alexandre-César-Léopold Bizet, conocido como Georges Bizet (París, 25 de octubre de 1838-Bougival, 3 de junio de 1875), fue un compositor francés, principalmente de óperas. En una carrera cortada por su muerte prematura, alcanzó escasos éxitos hasta su última obra, Carmen, que se convirtió en una de las obras más populares e interpretadas de todo el repertorio operístico.\n
Bizet ganó varios premios a lo largo de su brillante carrera como estudiante en el Conservatorio de París, incluyendo el prestigioso Premio de Roma en 1857. Fue reconocido como un pianista excepcional, aunque prefirió no aprovechar su habilidad y en raras ocasiones tocó en público. Tras regresar a París después de pasar casi tres años en Italia, se dio cuenta de que en los principales teatros de ópera parisinos se prefería interpretar el repertorio clásico más arraigado antes que las obras de nuevos compositores. Sus composiciones orquestales y para teclado fueron asimismo ignoradas en su gran mayoría, lo que estancó su carrera por lo que tuvo que ganarse la vida principalmente mediante arreglos y transcripciones de la música de otros. En su busca del ansiado éxito, comenzó varios proyectos teatrales durante la década de 1860, muchos de los cuales abandonó. Ninguna de las dos óperas que se llegaron a escenificar —Los pescadores de perlas y La bella muchacha de Perth— tuvieron éxito de inmediato.\n
Tras la guerra franco-prusiana de 1870-71, en la que Bizet sirvió en la Guardia Nacional, tuvo cierto éxito con su ópera de un acto Djamileh, aunque la suite orquestal L'Arlésienne, derivada en su música incidental de la obra de teatro homónima de Alphonse Daudet, tuvo un éxito instantáneo. La producción de la última ópera de Bizet, Carmen, fue retrasada debido al miedo de que sus temas de traición y asesinato pudieran ofender a la audiencia. Tras su estreno el 3 de marzo de 1875, Bizet estaba convencido de que la obra iba a ser un fracaso; murió de un ataque al corazón tres meses más tarde, sin saber que resultaría un éxito espectacular y duradero.\n
Tras su muerte, sus composiciones, excepto Carmen, estuvieron en general desatendidas. Los manuscritos fueron regalados o se perdieron, y las versiones publicadas de sus obras a menudo eran revisadas y adaptadas por terceros. No creó escuela ni tuvo discípulos ni sucesores evidentes. Tras años de abandono, sus obras empezaron a ser interpretadas de nuevo con más frecuencia en el siglo xx. Críticos posteriores lo han proclamado como un compositor de gran brillantez y originalidad cuya muerte prematura supuso una gran pérdida para la música teatral francesa.\n""";
  String obrasBizet = """Carmen\nLos pescadores de perlas """;

  //Datos Chopin
  String bioChopin = """
Frédéric François Chopin,​ en polaco Fryderyk Franciszek Chopin, (Żelazowa Wola, Gran Ducado de Varsovia, 1 de marzo​ de 1810-París, 17 de octubre de 1849) fue un profesor, compositor y virtuoso pianista polaco, considerado uno de los más importantes de la historia y uno de los mayores representantes del Romanticismo musical. Su maravillosa técnica, su refinamiento estilístico y su elaboración armónica se han comparado históricamente, por su influencia en la música posterior, con las de Wolfgang Amadeus Mozart, Ludwig van Beethoven, Johannes Brahms, Franz Liszt o Serguéi Rajmáninov.\n
La descendencia pedagógica de Chopin llegó hasta pianistas como Maurizio Pollini y Alfred Cortot, por medio de Georges Mathias y Emile Descombes, respectivamente.\n
Si el piano es el instrumento romántico por excelencia se debe en gran parte a la aportación de Frédéric Chopin: en el extremo opuesto del pianismo orquestal de su contemporáneo Liszt (representante de la faceta más extrovertida y apasionada, casi exhibicionista, del Romanticismo), el compositor polaco exploró un estilo intrínsecamente poético, de un lirismo tan refinado como sutil, que aún no ha sido igualado.\n
Hijo de un maestro francés emigrado a Polonia, Chopin fue un niño prodigio que desde los seis años empezó a frecuentar los grandes salones de la aristocracia y la burguesía polacas, donde suscitó el asombro de los asistentes gracias a su sorprendente talento. De esa época datan también sus primeras incursiones en la composición.\n""";
  String obrasChopin = """Estudios Op. 10\nEstudios Op. 25\nTrois nouvelles études\nPolonesa Op. 53\n Nocturnos\nBaladas\n""";

  //Datos Clara Schumann
  String bioCSchumann = """
Clara Wieck (Leipzig, 13 de septiembre de 1819-Fráncfort del Meno, 20 de mayo de 1896), conocida como Clara Schumann, fue una pianista, compositora y profesora de piano alemana. Fue una de las grandes concertistas europeas del siglo xix y su carrera fue clave en la difusión de las composiciones de Robert Schumann, con quien estuvo casada. Considerada como una de las pianistas más distinguidas de la era romántica, ejerció su influencia en una carrera de conciertos de 61 años, cambiando el formato y el repertorio del recital de piano de exhibiciones de virtuosismo a programas de obras serias. También compuso piezas para piano en solitario, un Concierto para piano (su Op. 7), música de cámara, piezas corales y canciones.\n
Creció en Leipzig, donde su padre, Friedrich Wieck, era pianista y maestro profesional y su madre una cantante consumada. Fue una niña prodigio, entrenada por su padre. Comenzó a viajar a los once años y tuvo éxito en París y Viena, entre otras ciudades. Se casó con el compositor Robert Schumann y la pareja tuvo ocho hijos. Juntos, alentaron a Johannes Brahms y mantuvieron una estrecha relación con él. Estrenó muchas obras de su esposo y de Brahms en público.\n
Después de la muerte prematura de su marido, continuó sus giras de conciertos en Europa durante décadas, con frecuencia con el violinista Joseph Joachim y otros músicos de cámara. A partir de 1878, fue una influyente educadora de piano en el Conservatorio Hoch en Fráncfort, donde atrajo a estudiantes internacionales. Editó la publicación de la obra de su esposo. Murió en Fráncfort, pero fue enterrada en Bonn junto a su esposo.\n
Varias películas se han centrado en la vida de Schumann, como la primera Träumerei de 1944 o una de 2008, Geliebte Clara, dirigida por Helma Sanders-Brahms. Una imagen de Clara Schumann de una litografía de 1835 de Andreas Staub apareció en el billete de 100 marcos alemanes de 1989 a 2002. El interés en sus composiciones comenzó a resucitar a finales del siglo xx y su bicentenario de 2019 impulsó nuevos libros y exposiciones.\n""";
  String obrasCSchumann = """Concierto para piano en la menor, Op. 7.\nTrío para piano en sol menor 0p. 17.\n""";

  //Datos Dvorak
  String bioDvorak = """
Antonín Leopold Dvorak (Nelahozeves, 8 de septiembre de 1841-Praga, 1 de mayo de 1904). Hijo de un mesonero, ya desde niño demostró disposición para la música. Inició sus estudios en Zlonice en 1853 y los prosiguió en Praga durante el período 1857-59. Luego tocó la viola en una orquesta hasta 1871. Al mismo tiempo emprendió su actividad de compositor. El primer éxito alcanzado en este ámbito fue un Himno con texto de Viteslav Hálek (1873); gracias a tal obra obtuvo el cargo de organista de la iglesia de San Etelberto, que conservó hasta 1877.\n
En 1892 aceptó la invitación de marchar a Nueva York como director del Conservatorio Municipal; en América escribiría algunas de sus obras más famosas: la Sinfonía del Nuevo Mundo (1893), el Cuarteto en fa mayor (1893), los Cantos bíblicos (1894) y el Concierto para violoncelo y orquesta (1895). La nostalgia de la patria le indujo a regresar a Praga, donde volvió a ocupar el cargo de profesor de composición del Conservatorio, alcanzado en 1891.\n
Durante los últimos años de su vida intentó, sin mayor éxito, escribir para el teatro nacional, según el ejemplo de Bedrich Smetana; en este aspecto cabe recordar sobre todo Russalka (1900).n
Es considerado el principal representante del nacionalismo checo en la música. Está considerado como uno de los grandes compositores de la segunda mitad del siglo XIX. Sin perder una amplia proyección internacional, supo extraer las esencias de la música de su tierra natal.""";
  String obrasDvorak = """
Sinfonía n.º 1 \nSinfonía n.º 9 \nStabat Mater\nRéquiem \nEl jacobino \nEl diablo y Catalina \nRusalka \nConcierto para violonchelo \nDanzas eslavas \n""";

  //Datos F. Schubert
  String bioSchubert = """
Franz Peter Schubert (Viena, 31 de enero de 1797-ibidem, 19 de noviembre de 1828) fue un compositor austriaco de los principios del Romanticismo musical pero, a la vez, también continuador de la sonata clásica siguiendo el modelo de Ludwig van Beethoven. A pesar de su corta vida, Schubert dejó un gran legado, que incluye más de 600 obras vocales seculares (principalmente lieder), siete sinfonías completas, música sacra, óperas, música incidental y gran cantidad de obras para piano y música de cámara. Sus obras principales incluyen el Quinteto La trucha, la Sinfonía inacabada, la Sinfonía Grande, las tres últimas sonatas para piano (D. 958, 959 y 960), la ópera Fierrabras (D. 796), la música incidental de la obra de teatro Rosamunda (D. 797) y los ciclos de canciones La bella molinera (D. 795) y Viaje de invierno (D. 911).\n
  \tNOTA: La letra "D" que se encuentra entre paréntesis junto con varios números hace referencia al Catálogo de Deutsch, que es es una lista numerada de todas las composiciones de Franz Schubert, recopilada por el musicólogo Otto Erich Deutsch.\n
Nació en el suburbio Himmelpfortgrund de Viena y sus dotes poco comunes para la música fueron evidentes desde una edad temprana. Su padre le dio sus primeras lecciones de violín y su hermano mayor le dio lecciones de piano, pero Schubert pronto superó sus habilidades. En 1808, a la edad de once años, se convirtió en alumno en la escuela Stadtkonvikt, donde conoció la música orquestal de Joseph Haydn, Wolfgang Amadeus Mozart y Ludwig van Beethoven. Abandonó el Stadtkonvikt a finales de 1813 y regresó a su hogar para vivir con su padre, donde comenzó a estudiar para convertirse en maestro de escuela. A pesar de esto, continuó sus estudios de composición con Antonio Salieri. En 1821, la Gesellschaft der Musikfreunde lo admitió como miembro intérprete, lo que ayudó a que su nombre fuera conocido entre la ciudadanía vienesa. Dio un concierto de sus propias obras con gran éxito de crítica en marzo de 1828, la única vez que lo hizo en su carrera. Murió ocho meses después a la edad de 31 años y la causa oficialmente atribuida fue la fiebre tifoidea, aunque algunos historiadores creen que fue por sífilis.\n
La valoración de la música de Schubert mientras estaba vivo se limitó a un círculo relativamente pequeño de admiradores en Viena, pero el interés en su obra aumentó significativamente en las décadas posteriores a su muerte. Felix Mendelssohn, Robert Schumann, Franz Liszt, Johannes Brahms y otros compositores del siglo xix descubrieron y defendieron sus obras. Hoy, Schubert está clasificado entre los mejores compositores de música clásica occidental y su música sigue siendo popular.\n""";
  String obrasSchubert= """
Sinfonía n.º 1 \nSinfonía n.º 2 \n Sinfonía n.º 3 \nViaje de invierno \nCuarteto de cuerda n.º 14 \nEllens dritter Gesang\n""";

  //Datos Handel
  String bioHandel = """
Georg Friedrich Handel,  en inglés George Frideric (o Frederick) Handel (Halle, Brandeburgo-Prusia; 23 de febrero/5 de marzo de 1685 - Londres; 14 de abril de 1759) fue un compositor alemán, posteriormente nacionalizado inglés, considerado una de las figuras cumbre de la historia de la música, especialmente la barroca, y uno de los más influyentes compositores de la música occidental y universal. En la historia de la música, es el primer compositor moderno en haber adaptado y enfocado su música para satisfacer los gustos y necesidades del público, en vez de los de la nobleza y de los mecenas, como era habitual.\n
Considerado el sucesor y continuador de Henry Purcell,​ marcó toda una era en la música inglesa.​ Para varios expertos es el primer gran maestro de la música basada en la técnica de la homofonía y el más grande dentro del ámbito de los géneros de la ópera seria italiana y para algunos hasta en el oratorio, por delante de Johann Sebastian Bach.\n
Su legado musical, síntesis de los estilos alemán, italiano, francés e inglés de la primera mitad del siglo xviii, incluye obras en prácticamente todos los géneros de su época, donde 43 óperas, 26 oratorios (entre ellos El Mesías) y un legado coral son lo más sobresaliente e importante de su producción musical.\n""";
  String obrasHandel = """
El Mesías \nMúsica acuática \nMúsica para los reales fuegos de artificio\nJulio César en Egipto \nAlcina \nSerse""";


  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          fondoPantalla(),
          SafeArea(
            child: Column(
              children: [
                _miAppBar(context),
                Expanded(child: Container()),
                mozartBio(),
                Expanded(child: Container()),
                listaMusicos(context),
                Expanded(child: Container()),
              ],
            ),
          ),

        ],
      ),
    );
  }

  Widget fondoPantalla(){
    return Container(
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment(0.0,0.0),
          end: Alignment (0.5,0.9),
          colors: <Color>[
            Colors.amber[300],
            Colors.amber[100],
          ]
        )
      ),
    );
  }

  Widget _miAppBar(BuildContext context){
    return Container(
      height: 55,
      decoration: BoxDecoration(
        color: Colors.amber[600]
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children:<Widget> [
          Container(
            padding: EdgeInsets.only(left:20, top: 10),
            width: 50,
              child: Icon(
                Icons.music_note,
                size: 35,
                color: Colors.black,
              ),
          ),
          SizedBox(width: 75),
          _fadeText()  
        ],
      ),
    );
  }

  Widget _fadeText(){
    return Center(
      child: Container(
        child: ColorizeAnimatedTextKit(
          repeatForever: true, 
          text: [
            "¡Bienvenido!",
            "Clasi-Pedia",
          ],
          textStyle: GoogleFonts.abrilFatface(
            fontSize: 30.0, 
            fontWeight: FontWeight.w600,
          ),
          colors: [
            Colors.grey[900],
            Colors.grey[850],
            Colors.grey[800],
            Colors.grey[700],
          ],
          textAlign: TextAlign.center,
          alignment: AlignmentDirectional.topStart // or Alignment.topLeft
        ),
      ),
    );
  }

  Widget mozartBio(){
    return Center(
      child: Container(
        height: 435,
        width: 350,
        child: Stack(
          children: [
            Positioned(
              top: 5,
              child: ClipRRect(
                borderRadius: BorderRadius.circular(20),
                child: FadeInImage.assetNetwork(
                  placeholder: 'assets/Gifs/loading.gif', 
                  image: urlMozart,
                  width: 350,
                ),
              ),
            ),

            Positioned(
              top: 165,
              left: 10,
                child: Container(
                width: 330,
                height: 270,
                decoration: BoxDecoration(
                  color: Colors.purple[400],
                  borderRadius: BorderRadius.circular(10)
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [

                    Container(
                      margin: EdgeInsets.only(left: 10, right: 10),
                      child: Text('Wolfgang Amadeus Mozart',
                        style: GoogleFonts.greatVibes(
                          fontSize: 28, 
                          color: Colors.yellow[50]
                        )
                      ),
                    ),

                    Container(
                      margin: EdgeInsets.only(left: 10, right: 10),
                      child: Text(
                        bioMozart,
                        textAlign: TextAlign.justify,
                        style: GoogleFonts.alegreya(
                          fontSize: 16, 
                          color: Colors.yellow[50]
                        ),
                      ),
                    ),

                    Center(
                      child: FlatButton(
                        color: Colors.grey[850],
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(30)
                        ),
                        onPressed: (){
                          Navigator.of(context).push(
                            MaterialPageRoute(
                              builder: (context)=> BioX(
                                musico: 'Mozart',
                                nombreCompleto: '\nWolfgang Amadeus Mozart\n',
                                bioMusico: bioMozartComp,
                                fotoMusico: 'assets/Musicos/mozart.jpg',
                                obras: obrasMozart,
                              )
                            )
                          );
                        },
                        
                        child: Text(
                          'Ver mas', 
                          style: TextStyle(
                            fontSize: 18, 
                            color: Colors.white
                          ),
                        ),
                    ),
                    ),
                  ],
                ),
              ),
            )
          ]
        )
      ),
    );
  }

  Widget listaMusicos(BuildContext context){
    return Center(
      child: Container(
        height: 170,
        width: 380,
        child: ListView(
          scrollDirection: Axis.horizontal,
          children: [

            Container(

              margin: EdgeInsets.all(5),
              width: 150,
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage('assets/Musicos/beethoven.jpg'),
                  fit: BoxFit.cover,
                ),
                borderRadius: BorderRadius.circular(10),
              ),

              child: Container(
                child: FlatButton(
                  onPressed: (){
                    Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: (context)=> BioX(
                          musico: 'Beethoven',
                          nombreCompleto: '\nLudwig van Beethoven\n',
                          bioMusico: bioBeethoven,
                          fotoMusico: 'assets/Musicos/beethoven.jpg',
                          obras: obrasBeethoven,
                        )
                      )
                    );
                  },
                        
                  child: Padding(
                    padding: EdgeInsets.only(top: 130, bottom: 5),
                    child: Container(
                      width: 100,
                      decoration: BoxDecoration(
                        color: Colors.purple[400],
                        borderRadius: BorderRadius.circular(20),
                      ),
                      child: Center(
                        child: Text(
                          'Beethoven',
                          style: GoogleFonts.alegreya(
                            fontSize: 15,
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                  ),
                )
              ),

            ),
            //Container Beethoven

            Container(

              margin: EdgeInsets.all(5),
              width: 150,
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage('assets/Musicos/bizet.jpg'),
                  fit: BoxFit.cover,
                ),
                borderRadius: BorderRadius.circular(10),
              ),

              child: Container(
                child: FlatButton(

                  onPressed: (){
                    Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: (context)=> BioX(
                          musico: 'Bizet',
                          nombreCompleto: '\nGeorges Bizet\n',
                          bioMusico: bioBizet,
                          fotoMusico: 'assets/Musicos/bizet.jpg',
                          obras: obrasBizet,
                        )
                      )
                    );
                  },

                  child: Padding(
                    padding: EdgeInsets.only(top: 130, bottom: 5),
                    child: Container(
                      width: 100,
                      decoration: BoxDecoration(
                        color: Colors.purple[400],
                        borderRadius: BorderRadius.circular(20),
                      ),
                      child: Center(
                        child: Text(
                          'Bizet',
                          style: GoogleFonts.alegreya(
                            fontSize: 15,
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                  ),
                )
              ),

            ),
            //Container Bizet

            Container(

              margin: EdgeInsets.all(5),
              width: 150,
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage('assets/Musicos/chopin.jpg'),
                  fit: BoxFit.cover,
                ),
                borderRadius: BorderRadius.circular(10),
              ),

              child: Container(
                child: FlatButton(

                  onPressed: (){
                    Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: (context)=> BioX(
                          musico: 'Chopin',
                          nombreCompleto: '\nFrédéric Chopin\n',
                          bioMusico: bioChopin,
                          fotoMusico: 'assets/Musicos/chopin.jpg',
                          obras: obrasChopin,
                        )
                      )
                    );
                  },

                  child: Padding(
                    padding: EdgeInsets.only(top: 130, bottom: 5),
                    child: Container(
                      width: 100,
                      decoration: BoxDecoration(
                        color: Colors.purple[400],
                        borderRadius: BorderRadius.circular(20),
                      ),
                      child: Center(
                        child: Text(
                          'Chopin',
                          style: GoogleFonts.alegreya(
                            fontSize: 15,
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                  ),
                )
              ),

            ),
            //Container Chopin
            
            Container(

              margin: EdgeInsets.all(5),
              width: 150,
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage('assets/Musicos/claraSchumann.jpg'),
                  fit: BoxFit.cover,
                ),
                borderRadius: BorderRadius.circular(10),
              ),

              child: Container(
                child: FlatButton(

                  onPressed: (){
                    Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: (context)=> BioX(
                          musico: 'Clara Schumann',
                          nombreCompleto: '\nClara Schumann\n',
                          bioMusico: bioCSchumann,
                          fotoMusico: 'assets/Musicos/claraSchumann.jpg',
                          obras: obrasCSchumann,
                        )
                      )
                    );
                  },

                  child: Padding(
                    padding: EdgeInsets.only(top: 130, bottom: 5),
                    child: Container(
                      width: 100,
                      decoration: BoxDecoration(
                        color: Colors.purple[400],
                        borderRadius: BorderRadius.circular(20),
                      ),
                      child: Center(
                        child: Text(
                          'C. Schumann',
                          style: GoogleFonts.alegreya(
                            fontSize: 15,
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                  ),
                )
              ),

            ),
            //Container Clara Schumann

            Container(

              margin: EdgeInsets.all(5),
              width: 150,
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage('assets/Musicos/dvorak.jpg'),
                  fit: BoxFit.cover,
                ),
                borderRadius: BorderRadius.circular(10),
              ),

              child: Container(
                child: FlatButton(

                  onPressed: (){
                    Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: (context)=> BioX(
                          musico: 'Dvorak',
                          nombreCompleto: '\nAntonín Leopold Dvorak\n',
                          bioMusico: bioDvorak,
                          fotoMusico: 'assets/Musicos/dvorak.jpg',
                          obras: obrasDvorak,
                        )
                      )
                    );
                  },

                  child: Padding(
                    padding: EdgeInsets.only(top: 130, bottom: 5),
                    child: Container(
                      width: 100,
                      decoration: BoxDecoration(
                        color: Colors.purple[400],
                        borderRadius: BorderRadius.circular(20),
                      ),
                      child: Center(
                        child: Text(
                          'Dvorak',
                          style: GoogleFonts.alegreya(
                            fontSize: 15,
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                  ),
                )
              ),

            ),
            //Container Dvorak

            Container(

              margin: EdgeInsets.all(5),
              width: 150,
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage('assets/Musicos/franzSchubert.jpg'),
                  fit: BoxFit.cover,
                ),
                borderRadius: BorderRadius.circular(10),
              ),

              child: Container(
                child: FlatButton(

                  onPressed: (){
                    Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: (context)=> BioX(
                          musico: 'F. Schubert',
                          nombreCompleto: '\nFranz Peter Schubert\n',
                          bioMusico: bioSchubert,
                          fotoMusico: 'assets/Musicos/franzSchubert.jpg',
                          obras: obrasSchubert,
                        )
                      )
                    );
                  },

                  child: Padding(
                    padding: EdgeInsets.only(top: 130, bottom: 5),
                    child: Container(
                      width: 100,
                      decoration: BoxDecoration(
                        color: Colors.purple[400],
                        borderRadius: BorderRadius.circular(20),
                      ),
                      child: Center(
                        child: Text(
                          'F. Schubert',
                          style: GoogleFonts.alegreya(
                            fontSize: 15,
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                  ),
                )
              ),

            ),
            //Container Schubert

            Container(

              margin: EdgeInsets.all(5),
              width: 150,
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage('assets/Musicos/handel.jpg'),
                  fit: BoxFit.cover,
                ),
                borderRadius: BorderRadius.circular(10),
              ),

              child: Container(
                child: FlatButton(

                  onPressed: (){
                    Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: (context)=> BioX(
                          musico: 'Handel',
                          nombreCompleto: '\nGeorg Friedrich Handel\n',
                          bioMusico: bioHandel,
                          fotoMusico: 'assets/Musicos/handel.jpg',
                          obras: obrasHandel, 
                        )
                      )
                    );
                  },

                  child: Padding(
                    padding: EdgeInsets.only(top: 130, bottom: 5),
                    child: Container(
                      width: 100,
                      decoration: BoxDecoration(
                        color: Colors.purple[400],
                        borderRadius: BorderRadius.circular(20),
                      ),
                      child: Center(
                        child: Text(
                          'Handel',
                          style: GoogleFonts.alegreya(
                            fontSize: 15,
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                  ),
                )
              ),

            ),
            //Container Handel

            Container(

              margin: EdgeInsets.all(5),
              width: 100,
              decoration: BoxDecoration(
                color: Colors.purple[300],
                borderRadius: BorderRadius.circular(10),
              ),

              child: Container(
                child: IconButton(
                  icon: Icon(
                    Icons.add_circle_outline,
                    color: Colors.black,
                    size: 80,
                  ), 
                  onPressed: (){
                    Navigator.push(
                      context, 
                      MaterialPageRoute(
                        builder: (BuildContext context)=>MasMusicos(), 
                      )
                    );
                  }
                )
              ),
            ),
            //Container Mas Musicos
            
          ],
        ),
      ),
    );
  }
}
