import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:animated_text_kit/animated_text_kit.dart';

class BioX extends StatefulWidget {

  BioX({
    this.musico,
    this.nombreCompleto,
    this.bioMusico,
    this.fotoMusico,
    this.obras,
  });

  final String musico;
  final String bioMusico;
  final String nombreCompleto;
  final String fotoMusico;
  final String obras;

  @override
  _BioXState createState() => _BioXState();
}

class _BioXState extends State<BioX> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          fondoPantalla(),
          SafeArea(
            child: Column(
              children: [
                _miAppBar(context),
                Flexible(
                  child: ListView(
                    padding: EdgeInsets.all(20),
                    children: [
    
                      Container(
                        width: 250,
                        height: 250,
                        margin: EdgeInsets.all(5),
                        decoration: BoxDecoration(
                          image: DecorationImage(
                            image: AssetImage(widget.fotoMusico),
                            fit: BoxFit.cover,
                          ),
                          borderRadius: BorderRadius.circular(10),
                        ),
                      ),
                      //Foto

                      Container(
                        margin: EdgeInsets.only(left: 10, right: 10),
                        child: Text(widget.nombreCompleto,
                          style: GoogleFonts.greatVibes(
                            fontSize: 28, 
                            color: Colors.black,
                          )
                        ),
                      ),
                      //Nombre

                      Container(
                        margin: EdgeInsets.only(left: 10, right: 10),
                        child: Text(
                          widget.bioMusico,
                          textAlign: TextAlign.justify,
                          style: GoogleFonts.alegreya(
                            fontSize: 16, 
                            color: Colors.black
                          ),
                        ),
                      ),
                      //Biografia

                      Container(
                        margin: EdgeInsets.only(left: 10, right: 10),
                        child: Text(
                          'Obras destacadas:',
                          style: GoogleFonts.alegreya(
                            fontSize: 20, 
                            color: Colors.black
                          ),
                        ),
                      ),
                      //Título Obras

                      Container(
                        margin: EdgeInsets.only(left: 10, right: 10),
                        child: Text(
                          widget.obras,
                          textAlign: TextAlign.justify,
                          style: GoogleFonts.alegreya(
                            fontSize: 16, 
                            color: Colors.black
                          ),
                        ),
                      ),
                      //Obras destacadas
                    ],
                  ),
                  
                ),
                
              ],
            ),
          ),

        ],
      ),
    );
  }

  Widget fondoPantalla(){
    return Container(
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment(0.0,0.0),
          end: Alignment (0.5,0.9),
          colors: <Color>[
            Colors.amber[300],
            Colors.amber[100],
          ]
        )
      ),
    );
  }

  Widget _miAppBar(BuildContext context){
    return Container(
      height: 55,
      decoration: BoxDecoration(
        color: Colors.amber[600]
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children:<Widget> [
          Container(
            width: 50,
            child: FlatButton(
              padding: EdgeInsets.only(left:10, top: 10),
              onPressed: (){
                Navigator.pop(context);
              },
              child: Icon(
                Icons.arrow_back,
                size: 35,
                color: Colors.black,
              ),
            ),
          ),
          SizedBox(width: 25),
          _fadeText()  
        ],
      ),
    );
  }

  Widget _fadeText(){
    return Center(
      child: Container(
        child: ColorizeAnimatedTextKit(
          repeatForever: true, 
          text: [
            "Clasi-Pedia",
            widget.musico,
          ],
          textStyle: GoogleFonts.abrilFatface(
            fontSize: 30.0, 
            fontWeight: FontWeight.w600,
          ),
          colors: [
            Colors.grey[900],
            Colors.grey[850],
            Colors.grey[800],
            Colors.grey[700],
          ],
          textAlign: TextAlign.center,
          alignment: AlignmentDirectional.topStart // or Alignment.topLeft
        ),
      ),
    );
  }

}